# Intro

I wanted to play with node js and this is how Flying Superman project appeared.

Flying Superman enables the user to create a *screen* spanning several devices and he can fly across all of them.


The project uses:

* npm for package managing
* express for the API
* express-session for maintaining session 
* connect-redis is used for session persistance
* socket.io for communication across browser/devices
* mongoose for accessing mongodb
* mocha, should and sinon are used for unit tests
* supertest, supertest-session are used for integration tests
* gulp for dev task management
* docker-compose for bringing up test environment

At the client side it uses:

* jquery for this and that
* greensock for animations


# Demo

Check this video file https://bitbucket.org/vbaruh/flyingsuperman/raw/534c10979a454991e085bd855b69cfe72af86066/src/main/js/static/flying_superman_demo.mp4
