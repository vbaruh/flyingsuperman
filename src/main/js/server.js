var express = require('express'),
    HttpServer = require('http').Server,
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    sessionHelper = require('./sesssionHelper'),
    WebScreenApp = require('./app'),
    SocketIoApp = require('./socketIoApp'),
    debug = require('debug')('app.server');

function _initEnv() {
  result = {};
  result.PORT = process.env.PORT || '3000';
  result.MONGO_URL = process.env.MONGODB_URI || process.env.MONGO_URL;
  result.REDIS_URI = process.env.REDIS_URI;
  result.ROOT_PATH = process.env.ROOT_PATH || '/';
  return result;
}

var env = _initEnv();
debug('env: %o', env);

// create session middleware
var session = sessionHelper.createSessionMiddleware(env.REDIS_URI);

// create web app
var app = WebScreenApp(env.ROOT_PATH, session);

// create http server
var httpServer = HttpServer(app);

// create socketIo
var io = SocketIoApp(httpServer, session);

// start mongo
mongoose.connect(env.MONGO_URL);

// start http server
httpServer.listen(env.PORT, function () {
  console.log('Example app listening on port ' + env.PORT + '.');

  var gracefulShutdown = function () {
    httpServer.close(function () {
      console.log('\nExit gracefully.');
      process.exit(0);
    });
  };

  process.on('SIGINT', function () {
    gracefulShutdown();
  });

  process.on('SIGTERM', function () {
    gracefulShutdown();
  });
});
