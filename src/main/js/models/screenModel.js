var mongoose = require('mongoose'),
    uuid = require('uuid/v4');

var screenModelSchema = mongoose.Schema({
  id: { type: String },
  created: { type: Date },
  devices: [ { type: String } ]
});

// used for instantiating new documents
// because this method could be stubbed.
// new ScreenModel() cannot be stubbed
screenModelSchema.statics.newInstance = function (object) {
  var result = this(object);
  result.id = uuid();
  result.created = new Date();
  return result;
};

var screenModel = mongoose.model('Screen', screenModelSchema);

module.exports = screenModel;