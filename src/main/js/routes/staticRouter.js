var express = require('express'),
    path = require('path'),
    debug = require('debug')('app');

var staticRoutes = function() {
  var staticContentPath = path.join(__dirname, '..', 'static');
  debug('static content path: %s', staticContentPath);

  var staticRouter = express.Router();
  staticRouter.use(express.static(staticContentPath));

  return staticRouter;
};

module.exports = staticRoutes;