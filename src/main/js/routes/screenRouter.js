var express = require('express'),
    path = require('path');

var screenRoutes = function(ScreenModel) {
  var controllersPath = '../controllers';

  var screenControllerPath = path.join(controllersPath, 'screenController');

  var screenRouter = express.Router(),
      screenController = require(screenControllerPath)(ScreenModel);

  screenRouter.route('/')
    .get(screenController.getInfo)
    .delete(screenController.leaveScreen);

  screenRouter.route('/create')
    .post(screenController.createScreen);

  screenRouter.route('/join')
    .post(screenController.joinScreen);

  screenRouter.route('/device')
    .post(screenController.createDevice)
    .delete(screenController.deleteDevice);

  return screenRouter;
};

module.exports = screenRoutes;