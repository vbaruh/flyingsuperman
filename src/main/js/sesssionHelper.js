var session = require('express-session'),
    debug = require('debug')('app.sessionHelper');

var sessionHelper = function() {
  var _prepareSession = function (redisUri) {
    debug('redis uri: %o', redisUri);
    var sessionStore = null;
    if (redisUri) {
      var RedisStore = require('connect-redis')(session);
      sessionStore = new RedisStore({ 'url': redisUri });
    }

    return session({
      store: sessionStore,
      secret: '0b84ea15-0a9d-4a7b-8829-c8f2b749b1ab',
      saveUninitialized: false,
      resave: true
    });
  };

  return {
    createSessionMiddleware: _prepareSession
  };
};

module.exports = sessionHelper();
