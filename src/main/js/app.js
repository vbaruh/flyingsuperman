var express = require('express'),
    session = require('express-session'),
    morgan = require('morgan'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    path = require('path'),
    debug = require('debug')('app');

var application = function(rootPath, sessionMiddleware) {
  var app = express();
  app.use(sessionMiddleware);

  // app.use(morgan('combined'));
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(bodyParser.json());

  var ScreenModel =  require('./models/screenModel');

  var screenRouter = require('./routes/screenRouter')(ScreenModel);
  var apiRootPath = path.join(rootPath, 'api', 'screen');
  app.use(apiRootPath, screenRouter);

  var staticRouter = require('./routes/staticRouter')();
  app.use(rootPath, staticRouter);

  return app;
};

module.exports = application;
