function setObjectVisible($object, visible) {
  if (visible) {
    $object.show();
  } else {
    $object.hide();
  }
}

function setSupermanAtWelcomePosition() {
  var $superman = $('#superman');
  var $window = $(window);
  var y = $window.innerHeight()/2  - $superman.height();
  TweenLite.set($superman, { x: 10, y: y, alpha: 1 });
  var fromX = 0 - $superman.width();
  TweenLite.from($superman, 1, { x: fromX, autoAlpha: 0 });
}

function setSupermanAtStartPosition() {
  var $superman = $('#superman');
  var $window = $(window);
  var y = $window.innerHeight()/2  - $superman.height();
  var toX = 0 - $superman.width();
  TweenLite.to($superman, 1, { x: toX, autoAlpha: 1 });
}

function hideMenu() {
  var $menu = $('#menu');
  TweenLite.to($menu, 0.5, { autoAlpha: 0 });
}

function showMenu() {
  var $menu = $('#menu');
  TweenLite.to($menu, 0.5, { autoAlpha: 1 });
}

function updateButtonVisibility(info) {
  var screenIsEmpty = (!info.screenId);

  setObjectVisible($('#createScreenButton'), screenIsEmpty);
  setObjectVisible($('#joinScreenButton'), screenIsEmpty);
  setObjectVisible($('#welcome'), screenIsEmpty);

  setObjectVisible($('#leaveScreenButton'), !screenIsEmpty);
  setObjectVisible($('#startAnimationButton'), !screenIsEmpty);
  setObjectVisible($('#screenInfo'), !screenIsEmpty);

  if (screenIsEmpty) {
    setSupermanAtWelcomePosition();
  } else {
    setSupermanAtStartPosition();
  }

  var screenIdText = "none";
  if (info.screenId) {
    screenIdText = info.screenId;
  }
  $('#screenIdLabel').text('Screen ID: ' + screenIdText);

  var deviceIdText = "none";
  if (info.deviceId) {
    deviceIdText = info.deviceId;
  }
  $('#deviceIdLabel').text('Device ID: ' + deviceIdText);
}

function joinScreen() {
  var screenIdToJoin = $('#screenIdToJoinEdit').val();
  if (!screenIdToJoin) {
    alert("Enter screen id in the input.");
  } else {
    screenApiClient.joinScreen(screenIdToJoin, function (info) {
      socketIoApiClient.connect(info.screenId, info.deviceId);
      updateButtonVisibility(info);
    });
  }
}

function createScreen() {
  screenApiClient.createScreen(function (info) {
    socketIoApiClient.connect(info.screenId, info.deviceId);
    updateButtonVisibility(info);
  });
}

function leaveScreen() {
  screenApiClient.leaveScreen(function (info) {
    socketIoApiClient.disconnect();
    updateButtonVisibility(info);
  });
}

function initiateAnimation() {
  socketIoApiClient.initiateAnimation();
  executeAnimation(null);
}

function executeAnimation(eventData) {
  var $superman = $('#superman');
  var $noop = $('#noop');

  var startLeft = 0 - $superman.width();
  var startTop = window.innerHeight/2 - $superman.height();
  TweenLite.set($superman, { x: startLeft, y: startTop });

  var endLeft = window.innerWidth + $superman.width();

  var speed = 200;
  var duration = window.innerWidth / 200;
  var firstDeviceId = eventData == null ?
                      screenApiClient.getDeviceId() :
                      eventData.firstDeviceId;


  var timeline = new TimelineLite({ paused: true });
  timeline.to($superman, duration, {
    delay: 0,
    x: endLeft,
    ease: "linear",

  });
  timeline.to($noop, 0, {delay: -1, x: 0, onComplete: function () {
      socketIoApiClient.startNext(firstDeviceId);
    }});

  timeline.play();
}

function prepareForAnimation(eventData) {
  hideMenu();
}

function completedAnimation(eventData) {
  showMenu();
}

function init() {
  $('#deviceIdLabel').hide();

  setSupermanAtWelcomePosition();
  showMenu();

  socketIoApiClient.on('startAnimation', executeAnimation);
  socketIoApiClient.on('prepareForAnimation', prepareForAnimation);
  socketIoApiClient.on('completedAnimation', completedAnimation);
  screenApiClient.getInfo(function (info) {
    if (info.screenId) {
      socketIoApiClient.connect(info.screenId, info.deviceId);
    }
    updateButtonVisibility(info);
  });
}
