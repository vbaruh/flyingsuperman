var socketIoApiClient = (function() {
  var self = this;
  self.sio = null;
  self.eventHandlers = {};

  _connect = function (screenId, deviceId) {
    self.screenId = screenId;
    self.deviceId = deviceId;
    self.sio = io();
    self.sio.emit('joinScreenRoom', self.screenId);

    self.sio.on('startAnimationNow', _onStartAnimationNow);
    self.sio.on('startAnimation', _onStartAnimation);
    self.sio.on('prepareForAnimation', _onPrepareForAnimation);
    self.sio.on('completedAnimation', _onCompletedAnimation);
  };

  _disconnect = function () {
    if (self.sio) {
      self.sio.close();
      self.sio = null;
    }
  };

  _onStartAnimation = function (eventData) {
    if (eventData &&
        eventData.deviceIdToStart &&
        eventData.deviceIdToStart == self.deviceId) {
     if (self.eventHandlers.startAnimation)  {
        self.eventHandlers.startAnimation(eventData);
      }
    }
  };

  _onStartAnimationNow = function (eventData) {
    console.log('startAnimationNow event occurred');

    if (self.eventHandlers.startAnimationNow)  {
      self.eventHandlers.startAnimationNow();
    }
  };

  _onPrepareForAnimation = function (eventData) {
    if (self.eventHandlers.prepareForAnimation)  {
      self.eventHandlers.prepareForAnimation(eventData);
    }
  };

  _onCompletedAnimation = function (eventData) {
    if (self.eventHandlers.completedAnimation)  {
      self.eventHandlers.completedAnimation(eventData);
    }
  };

  _initiateAnimation = function (durationSeconds) {
    self.sio.emit('initiateAnimation');
  };

  _startNext = function (firstDeviceId) {
    if (!firstDeviceId) {
      firstDeviceId = self.deviceId;
    }
    var eventData = {
      firstDeviceId: firstDeviceId,
      completedDeviceId: self.deviceId
    };

    self.sio.emit('startAnimationOnNextDevice', eventData);
  };

  _on = function (eventName, callback) {
    self.eventHandlers[eventName] = callback;
  };

  return {
    on: _on,
    connect: _connect,
    disconnect: _disconnect,
    initiateAnimation: _initiateAnimation,
    startNext: _startNext
  };

})();