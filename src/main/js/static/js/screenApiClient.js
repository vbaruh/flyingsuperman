var screenApiClient = (function (window) {
  var self = this;
  self.pageUrl = window.location.href;
  if (self.pageUrl[self.pageUrl.length - 1] != '/') {
    self.pageUrl = self.pageUrl + '/';
  }
  self.apiUrl = self.pageUrl + 'api/screen';

  self.infoObject = null;

  var _getInfo = function (callback) {
    $.get(self.apiUrl, function (data) {
      self.infoObject = data;
      if (callback) {
        callback(data);
      }
    });
  };

  var _createScreen = function (callback) {
    // create screen
    $.post(self.apiUrl + '/create', function (data) {
      self.infoObject = data;

      // create device
      _createDeviceLowLevel(data.screenId, function (data) {
        self.infoObject = data;
        if (callback) {
          callback(data);
        }
      });
    });
  };

  var _deleteDevice = function (callback) {
    $.ajax({
      url: self.apiUrl + '/device',
      method: 'delete',
      success: callback
    });
  };

  var _leaveScreenLowLevel = function (callback) {
    $.ajax({
      url: self.apiUrl,
      method: 'delete',
      success: callback
    });
  };

  var _joinScreenLowLevel = function (screenId, callback, errorCallback) {
    $.ajax({
      url: self.apiUrl + '/join',
      data: { screenId: screenId },
      method: 'post',
      success: callback,
      error: errorCallback
    });
  };

  var _createDeviceLowLevel = function (screenId, callback) {
    var createDeviceUrl = self.apiUrl + '/device';

    $.ajax({
      url: createDeviceUrl,
      method: 'post',
      success: callback
    });
  };

  var _leaveScreen = function (callback) {
    if (self.infoObject.deviceId) {
      _deleteDevice(function (data) {
        _leaveScreenLowLevel(callback);
      });
    } else {
      _leaveScreenLowLevel(callback);
    }
  };

  var _joinScreen = function (screenId, callback) {
    var joinScreenOnSuccess = function (data) {
      self.infoObject = data;
      _createDeviceLowLevel(data.screenId, function (data) {
        self.infoObject = data;
        if (callback) {
          callback(data);
        }
      });
    };

    var joinScreenOnFailure = function (data) {
      alert('Cannot join a screen with specified id.' +
            '\n * If the screen id is partial, add more characters.' +
            '\n * Check that the screen id is correctly entered.'
          );
    };

    _joinScreenLowLevel(screenId, joinScreenOnSuccess, joinScreenOnFailure);
  };

  var _getDeviceId = function () {
    return self.infoObject.deviceId;
  };

  return {
    getInfo: _getInfo,
    createScreen: _createScreen,
    leaveScreen: _leaveScreen,
    joinScreen: _joinScreen,

    getDeviceId: _getDeviceId
  };
})(window);