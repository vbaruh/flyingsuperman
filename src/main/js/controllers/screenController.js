var HTTP_STATUS = require('http-status-codes'),
    uuid = require('uuid/v4'),
    debug = require('debug')('app.screenController');

var screenController = function (ScreenModel) {
  var _getInfo = function (req, res) {
    res.status(HTTP_STATUS.OK);
    res.json(_createInfoObectFromSession(req.session));
  };

  var _createScreen = function (req, res) {
    if (_sessionContainsScreen(req.session)) {
      res.status(HTTP_STATUS.CREATED);
      res.json(_createInfoObectFromSession(req.session));
    } else {
      var screen = ScreenModel.newInstance({});
      screen.save(function (err, savedScreen) {
        if (err) {
          _error(res, HTTP_STATUS.INTERNAL_SERVER_ERROR, err);
        } else {
          _updateSessionScreenId(req.session, savedScreen.id);

          res.status(HTTP_STATUS.CREATED);
          res.json(_createInfoObectFromSession(req.session));
        }
      });
    }
  };

  var _createDevice = function (req, res) {
    if (!_sessionContainsScreen(req.session)) {
      _error(res, HTTP_STATUS.BAD_REQUEST, "Create or join screen first.");
    } else if (_sessionContainsDevice(req.session)) {
      res.status(HTTP_STATUS.CREATED);
      res.json(_createInfoObectFromSession(req.session));
    } else {
      ScreenModel.findOne({ id: req.session.data.screenId }, function (err, screen) {
        if (err) {
          _error(res, HTTP_STATUS.INTERNAL_SERVER_ERROR, err);
        } else {
          if (!screen) {
            _error(res, HTTP_STATUS.INTERNAL_SERVER_ERROR,
                   'The session`s screenId does not exist.');
          } else {
            var newDeviceId = uuid();
            screen.devices.push(newDeviceId);

            ScreenModel.findByIdAndUpdate(screen._id, screen,
                                          function (err, savedScreen) {
              if (err) {
                _error(res, HTTP_STATUS.INTERNAL_SERVER_ERROR, err);
              } else {
                _updateSessionDeviceId(req.session, newDeviceId);
                res.status(HTTP_STATUS.CREATED);

                res.json(_createInfoObectFromSession(req.session));
              }
            });
          }
        }
      });
    }
  };

  _joinScreen = function (req, res) {
    if (!req.body.screenId) {
      _error(res, HTTP_STATUS.BAD_REQUEST,
             "screenId param should be specified.");
    } else {
      var screenIdToJoin = req.body.screenId;
      if (_sessionContainsScreen(req.session)) {
        if (req.session.data.screenId === screenIdToJoin) {
          res.status(HTTP_STATUS.ACCEPTED);
          res.json(_createInfoObectFromSession(req.session));
        } else {
          _error(res, HTTP_STATUS.BAD_REQUEST,
                 "Current session is joined to another screen");
        }
      } else {
        var screenIdRegex = new RegExp('^' + screenIdToJoin);
        ScreenModel.find({ 'id': { $regex:  screenIdRegex } }, function (err, screens) {
          if (err) {
            _error(res, HTTP_STATUS.INTERNAL_SERVER_ERROR, err);
          } else {
            if (!screens || screens.length == 0) {
              _error(res, HTTP_STATUS.BAD_REQUEST, 'Specified screenId does not exist.');
            } else if (screens.length > 1) {
              _error(res, HTTP_STATUS.BAD_REQUEST, 'Specified screenId matches multiple screens.');
            } else {
              _updateSessionScreenId(req.session, screens[0].id);
              res.status(HTTP_STATUS.ACCEPTED);
              res.json(_createInfoObectFromSession(req.session));
            }
          }
        });


      }
    }
  };

  _deleteDevice = function (req, res) {
    if (!_sessionContainsScreen(req.session)) {
      _error(res, HTTP_STATUS.BAD_REQUEST,
             "Current session does not have screen.");
    } else if (!_sessionContainsDevice(req.session)) {
      _error(res, HTTP_STATUS.BAD_REQUEST,
              "Current session does not have device.");
    } else {
      ScreenModel.findOne({id: _getSessionScreenId(req.session) },
                           function (err, screen) {
        if (err) {
          _error(res, HTTP_STATUS.INTERNAL_SERVER_ERROR, err);
        } else {
          if (screen) {
            var found = -1;
            var deviceId = _getSessionDeviceId(req.session);
            for (var i = 0; i < screen.devices.length; i++) {
              if (screen.devices[i] == deviceId) {
                found = i;
                break;
              }
            }

            if (found !== -1) {
              screen.devices.splice(found, 1);
              debug('SCREEN WITH REMOVED DEVICE: %o', screen.devices);

              ScreenModel.findByIdAndUpdate(screen._id, screen,
                                           function (err, savedScreen) {
                if (err) {
                  _error(res, HTTP_STATUS.INTERNAL_SERVER_ERROR, err);
                } else {
                  _updateSessionDeviceId(req.session, null);

                  res.status(HTTP_STATUS.ACCEPTED);
                  res.json(_createInfoObectFromSession(req.session));
                }
              });
            } else {
              _error(res, HTTP_STATUS.INTERNAL_SERVER_ERROR,
                    "Sessions's device does not exist in db.");
            }
          } else {
            _error(res, HTTP_STATUS.INTERNAL_SERVER_ERROR,
              "Sessions's screen does not exist in db.");
          }
        }
      });
    }
  };

  _leaveScreen = function (req, res) {
    if (_sessionContainsScreen(req.session)) {
      if (!_sessionContainsDevice(req.session)) {
        _updateSessionScreenId(req.session, null);
        res.status(HTTP_STATUS.ACCEPTED);
        res.json({});
      } else {
        _error(res, HTTP_STATUS.BAD_REQUEST, "Current session has a device.");
      }
    } else {
      _error(res, HTTP_STATUS.NOT_FOUND, "Current session does not have screen.");
    }
  };

  /* not exposed private members */

  var _error = function (res, httpStatus, errorMessage) {
    debug('ERROR %o, %o', httpStatus, errorMessage);
    res.status(httpStatus);
    res.json({message: errorMessage});
  };

  var _createInfoObectFromSession = function (session) {
    var result = {};
    if (session && session.data) {

      if (session.data.screenId) {
        result.screenId = session.data.screenId;
      }

      if (session.data.deviceId) {
        result.deviceId = session.data.deviceId;
      }
    }

    return result;
  };

  var _sessionContainsScreen = function(session) {
    return (session && session.data && session.data.screenId);
  };

  var _sessionContainsDevice = function(session) {
    return (session && session.data && session.data.deviceId);
  };

  var _updateSessionScreenId = function(session, screenId) {
    if (!session.data) {
      session.data = {};
    }

    session.data.screenId = screenId;
  };

  var _getSessionScreenId = function (session) {
    return session.data.screenId;
  };

  var _updateSessionDeviceId = function(session, deviceId) {
    if (!session.data) {
      session.data = {};
    }

    session.data.deviceId = deviceId;
  };

  var _getSessionDeviceId = function (session) {
    return session.data.deviceId;
  };


  /*
   * Returning final object.
   */
  return {
    getInfo: _getInfo,
    createScreen: _createScreen,
    createDevice: _createDevice,
    joinScreen: _joinScreen,
    deleteDevice: _deleteDevice,
    leaveScreen: _leaveScreen
  };
};

module.exports = screenController;