var socketIo = require('socket.io'),
    ScreenModel = require('./models/screenModel'),
    debug = require('debug')('app.socketio');

var socketIoApp = function (httpServer, sessionMiddleware) {
  var sio = socketIo(httpServer);
  sio.use(function (socket, next) {
    sessionMiddleware(socket.request, socket.request.res, next);
  });

  var _debug = function () {
    var socket = arguments[0];
    var message = arguments[1];

    var sessionId = 'unknown';
    if (socket && socket.request && socket.request.session) {
      sessionId = socket.request.session.id;
    }
    var screenId = 'unknown';
    if (socket && socket.request && socket.request.session.data) {
      screenId = socket.request.session.data.screenId;
    }
    var args = Array.prototype.slice.call(arguments, 2);
    debug('[%s][%s] ' + message, sessionId, screenId, args);
  };

  var _emitEventToRoom = function (socket, event, eventData) {
    var room = socket.request.session.data.screenId;
    sio.sockets.in(room).emit(event, eventData);
  };

  sio.sockets.on('connection', function (socket) {
    socket.on('joinScreenRoom', function (room) {
      _debug(socket, "joined screen room %s", room);
      socket.join(room);
    });
  });

  sio.on('connect', function (socket) {
    if (!socket.request.session || !socket.request.session.data) {
      debug('user without session has connected.');
      return;
    }

    _debug(socket, 'user has connected. %s');

    socket.on('initiateAnimation', function (eventData) {
      _onInitiateAnimation(socket, eventData);
    });

    socket.on('startAnimationOnNextDevice', function (eventData) {
      _onStartAnimationOnNextDevice(socket, eventData);
    });

    socket.on('disconnect', function () {
      _debug(socket, 'a user has disconnected.');
    });
  });

  _onInitiateAnimation = function (socket, eventData) {
    _debug(socket, 'onInitiateAnimation %o', eventData);
    _emitEventToRoom(socket, 'prepareForAnimation', eventData);
  };

  _onStartAnimationOnNextDevice = function (socket, eventData) {
    _debug(socket, 'onStartAnimationOnNextDevice: %o', eventData);
    var screenId = socket.request.session.data.screenId;
    var deviceId = socket.request.session.data.deviceId;

    var onSuccess = function (screen) {
      if (screen) {
        var index = _findDeviceIndex(screen.devices, deviceId);
        var newIndex = index + 1;
        if (newIndex >= screen.devices.length) {
          newIndex = 0;
        }

        var newEventData = {
          firstDeviceId: eventData.firstDeviceId,
          deviceIdToStart: screen.devices[newIndex]
        };

        if (newEventData.firstDeviceId != newEventData.deviceIdToStart) {
          _debug(socket, 'Emit startAnimation message. ' +
                         'newIndex %s, newEventData %o',
                         newIndex, newEventData);
          _emitEventToRoom(socket, 'startAnimation', newEventData);
        } else {
          _debug(socket, 'Completed animation because reached the first device.');
          _emitEventToRoom(socket, 'completedAnimation');
        }
      } else {
        _debug(socket, "Session's screen does not exist in mongo");
      }
    };

    var onFailure = function (err) {
      _debug(socket, 'findScreen led to error %o', err);
    };

    _findScreen(screenId, onSuccess, onFailure);
  };

  var _findDeviceIndex = function (devices, deviceId) {
    debug('looking for index of %o in %o', deviceId, devices);
    var index = -1;
    for (var i=0; i < devices.length; i++) {
      if (devices[i] == deviceId) {
        index = i;
        break;
      }
    }

    return index;
  };

  var _findScreen = function (screenId, onSuccess, onError) {
    ScreenModel.findOne({id: screenId}, function (err, screen) {
      if (err) {
        debug('An error occurred while reading from mongo %o', err);
        if (onError) {
          onError(err);
        }
      } else {
        if (onSuccess) {
          onSuccess(screen);
        }
      }
    });
  };
};

module.exports = socketIoApp;