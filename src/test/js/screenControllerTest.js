var sinon = require('sinon'),
    should = require('should'),
    path = require('path'),
    util = require('util'),
    HTTP_STATUS = require('http-status-codes'),
    debug = require('debug')('test');

describe('ScreenController tests', function () {
  var mainDir = '../../main/js';

  var ScreenModel = require(path.join(mainDir, 'models/screenModel'));
  var ScreenController = require(path.join(mainDir,
                                          'controllers/screenController'));
  describe('getInfo tests', function () {
    it(('getInfo returns empty object when there is ' +
        'no screen in the session'), function (done) {
      _testGetInfo(null, null, done);
    });

    it('getInfo returns object with screenId.', function (done) {
      _testGetInfo('screenId', null, done);
    });

    it('getInfo returns object with screenId and deviceId.', function (done) {
      _testGetInfo('screenId', 'deviceId', done);
    });

    function _testGetInfo(screenId, deviceId, callback) {
      var sessionObject = _createSession(screenId, deviceId);
      var req = _createRequest(sessionObject);

      var screenController = _createScreenController();

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        // verification
        var actualResult = res.json.args[0][0];
        actualResult.should.be.deepEqual(sessionObject.data);

        _assertResponseStatus(HTTP_STATUS.OK, res);

        callback();
      });

      // test call
      screenController.getInfo(req, res);
    }
  });

  describe('createScreen tests', function () {
    it('createScreen creates and returns an object with screenId.', function (done) {
      var sessionObject = _createSession(null, null);
      var req = _createRequest(sessionObject);
      var screenController = _createScreenController();

      var screenDocument = {
        id: "createdScreenId",
        devices: [],
        created: new Date(),
        _id: "mongoId",

        save: function () {}
      };

      // stub ScreenModel.newInstance and Document.save calls
      var newInstanceStub = sinon.stub(ScreenModel, 'newInstance')
                                 .returns(screenDocument);

      var saveStub = sinon.stub(screenDocument, 'save');
      saveStub.callsArgWith(0, null, screenDocument);

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        var actualResult = res.json.args[0][0];
        var expectedObject = _createSession("createdScreenId", null);
        actualResult.should.be.deepEqual(expectedObject.data);

        _assertResponseStatus(HTTP_STATUS.CREATED, res);

        newInstanceStub.restore();
        done();
      });

      // test call
      screenController.createScreen(req, res);
    });

    it('createScreen called when there is a screen in the session ' +
       ' the screen from session and CREATED status.', function (done) {
      var sessionObject = _createSession("screenId", "deviceId");
      var req = { session: sessionObject };

      var screenController = _createScreenController();

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        var actualResult = res.json.args[0][0];
        actualResult.should.be.deepEqual(sessionObject.data);

        _assertResponseStatus(HTTP_STATUS.CREATED, res);
        done();
      });

      // test call
      screenController.createScreen(req, res);
    });
  });

  describe('createDevice tests', function () {
    it('createDevice returns an error if there is no ' +
       'screen in the session', function (done) {
      var sessionObject = _createSession(null, null);
      var req = { session: sessionObject };

      var screenController = _createScreenController();

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        _assertResponseStatus(HTTP_STATUS.BAD_REQUEST, res);

        var actualResult = res.json.args[0][0];
        actualResult.should.have.property('message');
        done();
      });

      // test call
      screenController.createDevice(req, res);
    });

    it('createDevice creates and returns the device', function (done) {
      var sessionObject = _createSession("screenId", null);
      var req = { session: sessionObject };
      var screenController = _createScreenController();

      var deviceDocument = {
        id: "createdDeviceId"
      };

      var screenDocument = {
        id: "screenId",
        created: new Date(),
        _id: "mongoId",
        devices: [],

        save: function () {}
      };

      var screenModelFindOneStub = sinon.stub(ScreenModel, 'findOne');
      screenModelFindOneStub.callsArgWith(1, null, screenDocument);

      var screenModelUpdateStub = sinon.stub(ScreenModel, 'findByIdAndUpdate');
      screenModelUpdateStub.callsArgWith(2, null, screenDocument);

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        var actualResult = res.json.args[0][0];
        actualResult.should.be.deepEqual(sessionObject.data);

        _assertResponseStatus(HTTP_STATUS.CREATED, res);

        screenModelFindOneStub.restore();
        screenModelUpdateStub.restore();
        done();
      });

      // test call
      screenController.createDevice(req, res);
    });


    it('createDevice called twice returns the same device', function (done) {
      var sessionObject = _createSession("screenId", "deviceId");
      var req = { session: sessionObject };

      var screenController = _createScreenController();

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        var actualResult = res.json.args[0][0];
        actualResult.should.be.deepEqual(sessionObject.data);

        _assertResponseStatus(HTTP_STATUS.CREATED, res);

        done();
      });

      // test call
      screenController.createDevice(req, res);
    });
  });

  describe('joinScreen tests', function () {
    it('joinScreen joins the specified screen.', function (done) {
      var sessionObject = _createSession(null, null);
      var expectedInfoObject = _createInfoObject("screenIdToJoin", null);
      var req = {
        body: { screenId: "screenIdToJoin"},
        session: sessionObject
      };

      var screenDocument = _createScreenDocumentStub("screenIdToJoin", []);

      var screenModelFindStub = sinon.stub(ScreenModel, 'find');
      screenModelFindStub.callsArgWith(1, null, [screenDocument]);

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        var actualResult = res.json.args[0][0];
        actualResult.should.be.deepEqual(expectedInfoObject);

        _assertResponseStatus(HTTP_STATUS.ACCEPTED, res);

        screenModelFindStub.restore();
        done();
      });

      // test call
      var screenController = _createScreenController();
      screenController.joinScreen(req, res);
    });

    // TODO: implement tests below
    it('joinScreen returns an error if no screen is matched', function (done) {
      var sessionObject = _createSession(null, null);
      var expectedInfoObject = _createInfoObject("screenIdToJoin", null);
      var req = {
        body: { screenId: "screenIdToJoin"},
        session: sessionObject
      };

      var screenDocument = _createScreenDocumentStub("screenIdToJoin", []);

      var screenModelFindStub = sinon.stub(ScreenModel, 'find');
      screenModelFindStub.callsArgWith(1, null, []);

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        _assertResponseStatus(HTTP_STATUS.BAD_REQUEST, res);

        screenModelFindStub.restore();
        done();
      });

      // test call
      var screenController = _createScreenController();
      screenController.joinScreen(req, res);
    });


    it('joinScreen returns an error if multiple screens are matched',
       function (done) {

      var sessionObject = _createSession(null, null);
      var expectedInfoObject = _createInfoObject("screenIdToJoin", null);
      var req = {
        body: { screenId: "screenIdToJoin"},
        session: sessionObject
      };

      var screenDocument = _createScreenDocumentStub("screenIdToJoin", []);

      var screenModelFindStub = sinon.stub(ScreenModel, 'find');
      screenModelFindStub.callsArgWith(1, null, ["screenId1","screenId2"]);

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        _assertResponseStatus(HTTP_STATUS.BAD_REQUEST, res);

        screenModelFindStub.restore();
        done();
      });

      // test call
      var screenController = _createScreenController();
      screenController.joinScreen(req, res);
    });


    it('joinScreen returns an error if the session already has a screen ' +
       ' but different from the specified screen.', function (done) {
        var sessionObject = _createSession("screenId", null);
        var req = {
          body: { screenId: "screenIdToJoin"},
          session: sessionObject
        };

        var screenController = _createScreenController();

        var res = _createResponseSpy();
        res.json = sinon.spy(function () {
          _assertResponseStatus(HTTP_STATUS.BAD_REQUEST, res);

          var actualResult = res.json.args[0][0];
          actualResult.should.have.property("message");

          done();
        });

        // test call
        screenController.joinScreen(req, res);
    });


    it('joinScreen called twice with the same screen returns success.',
        function (done) {
      var sessionObject = _createSession("screenId", "deviceId");
      var expectedInfoObject = sessionObject.data;
      var req = {
        body: { screenId: "screenId"},
        session: sessionObject
      };

      var screenController = _createScreenController();

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        var actualResult = res.json.args[0][0];
        actualResult.should.be.deepEqual(expectedInfoObject);

        _assertResponseStatus(HTTP_STATUS.ACCEPTED, res);

        done();
      });

      // test call
      screenController.joinScreen(req, res);
    });
  });

  describe('deleteDevice tests', function () {
    it('deleteDevice deletes the device from the session.', function (done) {
      var sessionObject = _createSession("screenId", "deviceId");
      var expectedInfoObject = _createInfoObject("screenId", null);
      var req = { session: sessionObject };

      var screenDocument = _createScreenDocumentStub("screenId", ["deviceId"]);
      var savedScreenDocument = _createScreenDocumentStub("screenId", []);

      var screenModelFindOneStub = sinon.stub(ScreenModel, 'findOne');
      screenModelFindOneStub.callsArgWith(1, null, screenDocument);

      var screenModelUpdateStub = sinon.stub(ScreenModel, 'findByIdAndUpdate');
      screenModelUpdateStub.callsArgWith(2, null, screenDocument);

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        var actualResult = res.json.args[0][0];
        actualResult.should.be.deepEqual(expectedInfoObject);

        _assertResponseStatus(HTTP_STATUS.ACCEPTED, res);

        screenModelFindOneStub.restore();
        screenModelUpdateStub.restore();
        done();
      });

      // test call
      var screenController = _createScreenController();
      screenController.deleteDevice(req, res);
    });


    it('deleteDevice returns 404 when there is no device in the session.',
       function (done) {
      var sessionObject = _createSession("screenId", null);
      var req = { session: sessionObject };

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        _assertResponseStatus(HTTP_STATUS.BAD_REQUEST, res);

        done();
      });

      // test call
      var screenController = _createScreenController();
      screenController.deleteDevice(req, res);
    });
  });

  describe('leaveScreen tests', function () {
    it('leaveScreen returns an error if the session has a device.',
        function (done) {
      var sessionObject = _createSession("screenId", "deviceId");
      var req = {
        params: { screenId: "screenId" },
        session: sessionObject
      };

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        _assertResponseStatus(HTTP_STATUS.BAD_REQUEST, res);

        done();
      });

      // test call
      var screenController = _createScreenController();
      screenController.leaveScreen(req, res);
    });


    it('leaveScreen removes the screen from the session.', function (done) {
      var sessionObject = _createSession("screenId", null);
      var req = {
        params: { screenId: "screenId" },
        session: sessionObject
      };

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        _assertResponseStatus(HTTP_STATUS.ACCEPTED, res);

        var actualResult = res.json.args[0][0];
        actualResult.should.be.deepEqual({});

        done();
      });

      // test call
      var screenController = _createScreenController();
      screenController.leaveScreen(req, res);
    });


    it('leaveScreen returns 404 when there is no screen in the session.',
       function (done) {
      var sessionObject = _createSession(null, null);
      var req = {
        params: { screenId: "screenId" },
        session: sessionObject
      };

      var res = _createResponseSpy();
      res.json = sinon.spy(function () {
        _assertResponseStatus(HTTP_STATUS.NOT_FOUND, res);


        done();
      });

      // test call
      var screenController = _createScreenController();
      screenController.leaveScreen(req, res);
    });
  });

  function _createInfoObject(screenId, deviceId) {
    var result = {};

    if (screenId) {
      result.screenId = screenId;
    }

    if (deviceId) {
      result.deviceId = deviceId;
    }

    return result;
  }

  function _createSession(screenId, deviceId) {
    return { data: _createInfoObject(screenId, deviceId) };
  }

  function _createRequest(session) {
    return { session: session };
  }

  function _createScreenController() {
    var screenController = ScreenController(ScreenModel);
    return screenController;
  }

  function _createResponseSpy() {
    return {
      status: sinon.spy(),
      json: sinon.spy()
    };
  }

  function _createScreenDocumentStub(screenId, devices) {
    return {
      _id: "mongoId",
      __v: 0,
      id: screenId,
      devices: devices,
      created: new Date(),

      save: function () {}
    };
  }

  function _assertResponseStatus(expectedStatus, responseSpy) {
    var actualStatus = responseSpy.status.args[responseSpy.status.callCount-1][0];
    actualStatus.should.be.equal(expectedStatus);
  }
});
