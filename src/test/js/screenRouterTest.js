var sinon = require('sinon'),
    should = require('should'),
    mongoose = require('mongoose'),
    path = require('path'),
    request = require('supertest'),
    requestSession = require('supertest-session'),
    util = require('util'),
    HTTP_STATUS = require('http-status-codes'),
    sessionHelper = require('../../main/js/sesssionHelper'),
    debug = require('debug')('test');

describe("screenRouter tests", function () {
  before(function (done) {
    mongoose.connect(process.env.MONGO_URL, done);
  });

  after(function(done) {
    mongoose.connection.dropDatabase(function () {
      mongoose.connection.close(done);
    });
  });

  describe("happy cases", function () {
    var app = null;
    var sessionMiddleware = null;
    var appSession = null;
    var ScreenModel = require('../../main/js/models/screenModel');

    before(function (done) {
      sessionMiddleware = sessionHelper.createSessionMiddleware();
      app = require('../../main/js/app')('/', sessionMiddleware);
      done();
    });

    beforeEach(function (done) {
      appSession = requestSession(app);
      mongoose.connection.db.dropDatabase(done);
    });

    it('getInfo', function (done) {
      appSession
        .get('/api/screen')
        .expect(HTTP_STATUS.OK)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .end(function(err, res) {
          if (err) throw err;

          var body = res.body;
          body.should.be.deepEqual({});

          done();
        });
    });


    it('createScreen + getInfo', function (done) {
      _createScreen(function (err, res) {
        if (err) throw err;

        var infoAfterCreate = res.body;

        _getScreen(function (err, res) {
          if (err) throw err;

          var body = res.body;
          body.should.be.deepEqual(infoAfterCreate);

          // verification in mongo
          ScreenModel.findOne({id: body.screenId}, function (err, mongoResult) {
            if (err) throw err;

            var screen = mongoResult.toObject();

            screen.id.should.be.equal(body.screenId);
            screen.devices.should.be.deepEqual([]);
            done();
          });
        });
      });
    });


    it('createScreen + createDevice + getInfo + deleteDevice', function (done) {
      _createScreen(function (err, res) {
        if (err) throw err;

        var infoAfterCreate = res.body;

        // create device
        _createDevice(function (err, res) {
          if (err) throw err;

          var infoAfterCreateDevice = res.body;

          // get info, verify it, then verify mongodb state
          _getScreen(function (err, res) {
            if (err) throw err;

            var body = res.body;
            body.should.be.deepEqual(infoAfterCreateDevice);

            // verify mongodb state - check there's a device in screen object
            ScreenModel.findOne({id: body.screenId}, function (err, mongoResult) {
              if (err) throw err;

              var screen = mongoResult.toObject();
              screen.id.should.be.equal(body.screenId);
              screen.devices.length.should.be.equal(1);
              screen.devices[0].should.be.equal(body.deviceId);

              // delete device and then verify mongodb state again
              _deleteDevice(function (err, res) {
                if (err) throw err;

                var body = res.body;
                body.should.be.deepEqual(infoAfterCreate);

                ScreenModel.findOne({id: body.screenId}, function (err, mongoResult) {
                  if (err) throw err;

                  var screen = mongoResult.toObject();
                  screen.id.should.be.equal(body.screenId);
                  screen.devices.length.should.be.equal(0);
                });

                done();
              });
            });
          });
        });
      });
    });

    it('joinScreen from another session', function (done) {
      var otherSession = requestSession(app);
      otherSession
        .post('/api/screen/create')
        .expect(HTTP_STATUS.CREATED)
        .end(function (err, res) {
          if (err) throw err;

          var otherSessionInfo = res.body;
          _joinScreen(otherSessionInfo.screenId, function (err, res) {
            if (err) throw err;

            var sessionInfo = res.body;
            sessionInfo.should.be.deepEqual(otherSessionInfo);

            _leaveScreen(function (err, res) {
              if (err) throw err;

              var info = res.body;
              info.should.be.deepEqual({});

              done();
            });

          });
        });
    });

    function _getScreen(callback) {
      appSession
        .get('/api/screen')
        .expect(HTTP_STATUS.OK)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .end(callback);
    }

    function _createDevice(callback) {
      appSession
        .post('/api/screen/device')
        .expect(HTTP_STATUS.CREATED)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .end(callback);
    }

    function _deleteDevice(callback) {
      appSession
        .delete('/api/screen/device')
        .expect(HTTP_STATUS.ACCEPTED)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .end(callback);
    }

    function _createScreen(callback) {
      appSession
        .post('/api/screen/create')
        .expect(HTTP_STATUS.CREATED)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .end(callback);
    }

    function _joinScreen(screenId, callback) {
      appSession
        .post('/api/screen/join')
        .send({ screenId: screenId })
        .expect(HTTP_STATUS.ACCEPTED)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .end(callback);
    }

    function _leaveScreen(callback) {
      appSession
        .delete('/api/screen/')
        .expect(HTTP_STATUS.ACCEPTED)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .end(callback);
    }
  });
});