var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    gulpMocha = require('gulp-mocha'),
    gulpEnv = require('gulp-env'),
    child_process = require('child_process'),
    minimist = require('minimist');


function exec_docker_compose_cmd (docker_compose_args, options) {
  if (!options) {
    options = {};
  }

  var execOptions = {
    'cwd': 'src/docker',
    'shell': '/bin/bash'

  };
  var cmd = 'docker-compose ' + docker_compose_args;
  child_process.exec(cmd,
                     execOptions,
                     function (err, stdout, stderr) {
    if (err) {
      console.log('The command ' + cmd + ' completed with error: ' + err);
      console.log('STDOUT is: ' + stdout);
      console.log('STDERR is: ' + stderr);
    } else {
      console.log('The command ' + cmd + ' completed successfully.');
      if (options.print_stdout) {
        console.log(stdout);
      }
      if (options.print_stderr) {
        console.log(stderr);
      }
    }
  });
}

gulp.task('test', function () {
  gulpEnv({
    vars: {
      MONGO_URL: 'mongodb://localhost/screendb_test',
      MONGO_SERVER_URL: 'mongodb://localhost/',
      DEBUG: 'test*, app*'
    }
  });
  gulp.src('src/test/js/**/*.js', {read: false})
      .pipe(gulpMocha({ exit: true }));
});

gulp.task('dev', function() {
  nodemon({
    watch: ['src/main/js/', 'src/test/js/'],
    ext: 'js',

    tasks: ['test'],

    script: 'src/main/js/server.js',
    env: {
      'PORT': '3000',
      'DEBUG': 'app*', //, express*, connect:redis',
      'REDIS_URI': 'redis://localhost:6379/',
      'MONGO_URL': 'mongodb://localhost/screendb1'
    },

    delay: 500,
    verbose: true
  }).
  on('restart', function () {
    console.log('restarting by gulp-nodemon');
  });
});

gulp.task('docker-start', function () {
  exec_docker_compose_cmd('up -d');
});

gulp.task('docker-stop', function () {
  exec_docker_compose_cmd('down');
});

gulp.task('docker-status', function () {
  exec_docker_compose_cmd('top', {print_stdout: true});
});

gulp.task('docker-compose', function () {
  var cmdArgs = minimist(process.argv.slice(2));
  exec_docker_compose_cmd(cmdArgs.cmd, { print_stdout: true });
});